package com.sablania.musicplayer;

import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.orm.SugarRecord;
import com.sablania.musicplayer.adapters.SongsAdapter;
import com.sablania.musicplayer.models.SongModel;
import com.sablania.musicplayer.utils.AppController;
import com.sablania.musicplayer.utils.Constants;
import com.sablania.musicplayer.utils.GenericRequest;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    SongsAdapter adapter;
    ProgressDialog progressDialog;
    BottomSheetBehavior bottomSheetBehavior;
    AppBarLayout appBarLayout;
    View bottomSheet;
    LinearLayout llTryAgain;
    Button btnTryAgain;
    TextView tvNoFavouriteSong;
    MenuItem favouritesMenuItem, searchMenuItem;

    List<SongModel> list, tempList;
    int bottomSheetPeekHeight;
    boolean cacheUsed, doubleBackToExitPressedOnce = false, viewingFavourite = false;

    String API = "http://starlord.hackerearth.com/studio";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Music Player");
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        initialize();
//        Stetho.initializeWithDefaults(this);

        btnTryAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fetchSongsData();
            }
        });
        bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (bottomSheetBehavior.getState() != BottomSheetBehavior.STATE_COLLAPSED) {
                    appBarLayout.setExpanded(false);
                    bottomSheet.findViewById(R.id.iv_play).setVisibility(View.GONE);
                } else {
                    appBarLayout.setExpanded(true, true);
                    bottomSheet.findViewById(R.id.iv_play).setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });

        fetchSongsData();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        favouritesMenuItem = menu.findItem(R.id.action_favourite);
        searchMenuItem = menu.findItem(R.id.action_search);

        // Associate searchable configuration with the SearchView
        final SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        final SearchView searchView =
                (SearchView) menu.findItem(R.id.action_search).getActionView();
//        searchView.setBackground(getResources().getDrawable(R.color.md_white_1000));
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));

//        searchView.setSubmitButtonEnabled(true);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                list.clear();

                if (newText.isEmpty()) {
                    list.addAll(tempList);
                    adapter.notifyDataSetChanged();
                    return true;
                }
                List<SongModel> filteredList = new ArrayList<>();
                for (SongModel sm : tempList) {
                    if (sm.getSong().toLowerCase().contains(newText.toLowerCase())) {
                        filteredList.add(sm);
                    }
                }
                list.addAll(filteredList);
                adapter.notifyDataSetChanged();
                return true;
            }
        });
        searchView.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    searchView.setQuery("", true);
                    Constants.hideInputKeyboard(MainActivity.this, searchView);
                }
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_favourite) {
            viewingFavourite = true;
            ArrayList temp = (ArrayList) SugarRecord.find(SongModel.class, "is_favourite = ?", "1");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            favouritesMenuItem.setVisible(false);
            searchMenuItem.setVisible(false);
            getSupportActionBar().setTitle("Favourite Songs");
            if (temp.isEmpty()) {
                list.clear();
                adapter.notifyDataSetChanged();
                tvNoFavouriteSong.setVisibility(View.VISIBLE);
            } else {
                list.clear();
                list.addAll(temp);
                adapter.notifyDataSetChanged();
            }
        } else if (id == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (viewingFavourite) {
            viewingFavourite = false;
            favouritesMenuItem.setVisible(true);
            searchMenuItem.setVisible(true);
            tvNoFavouriteSong.setVisibility(View.GONE);
            list.clear();
            list.addAll(tempList);
            adapter.notifyDataSetChanged();
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setTitle("Music Player");

            return;
        }

        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        String Click_Back_Again = getString(R.string.toast_click_back_again);
        Toast.makeText(this, Click_Back_Again, Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);

    }

    private void initialize() {
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        appBarLayout = (AppBarLayout) findViewById(R.id.app_bar);
        bottomSheet = findViewById(R.id.bottom_sheet);
        llTryAgain = (LinearLayout) findViewById(R.id.ll_try_again);
        btnTryAgain = (Button) findViewById(R.id.btn_try_again);
        tvNoFavouriteSong = (TextView) findViewById(R.id.tv_no_favourite);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);

        bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);
        bottomSheetPeekHeight = (int) (80 * getResources().getDisplayMetrics().density);
        bottomSheetBehavior.setPeekHeight(bottomSheetPeekHeight);
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);


        list = new ArrayList<>();
        tempList = new ArrayList<>();
        adapter = new SongsAdapter(this, list, bottomSheetBehavior, bottomSheet);
        recyclerView.setAdapter(adapter);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
                linearLayoutManager.getOrientation());
        recyclerView.addItemDecoration(dividerItemDecoration);

    }

    private void fetchSongsData() {
        progressDialog.show();
        cacheUsed = false;
        try {

            @SuppressWarnings("unchecked")
            GenericRequest genericRequest = new GenericRequest(Request.Method.GET,
                    API, String.class, null, new Response.Listener() {
                @Override
                public void onResponse(Object response) {
                    cacheUsed = true;
                    llTryAgain.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);
                    bottomSheet.setVisibility(View.VISIBLE);

                    Type listType = new TypeToken<ArrayList<SongModel>>() {
                    }.getType();
                    List<SongModel> resultData = new Gson().fromJson(response.toString(), listType);

                    for (int i = 0; i < resultData.size(); i++) {
                        List<SongModel> t = SugarRecord.find(SongModel.class, "url = ?", resultData.get(i).getUrl());
                        if (!t.isEmpty() && t.get(0).isFavourite()) {
                            resultData.get(i).setFavourite(true);
                        }
                    }
                    SugarRecord.saveInTx(resultData);

                    list.clear();
                    list.addAll(resultData);

                    tempList.clear();
                    tempList.addAll(list);

                    adapter.notifyDataSetChanged();
                    progressDialog.dismiss();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDialog.dismiss();
                    if (!cacheUsed) {
                        llTryAgain.setVisibility(View.VISIBLE);
                        recyclerView.setVisibility(View.GONE);
                        bottomSheet.setVisibility(View.GONE);
                    }
                    Constants.handleVolleyError(MainActivity.this, error, true);
                }
            }, null, false);

            AppController.getInstance(MainActivity.this).addToRequestQueue(genericRequest, "songs");
        } catch (Exception e) {
            progressDialog.dismiss();
            e.printStackTrace();
        }
    }


    @Override
    protected void onDestroy() {
        //This is to prevent memory leak
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
        super.onDestroy();
    }
}
