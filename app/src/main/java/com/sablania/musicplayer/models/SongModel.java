package com.sablania.musicplayer.models;

import com.google.gson.annotations.SerializedName;
import com.orm.dsl.Table;
import com.orm.dsl.Unique;

import java.io.Serializable;

@Table
public class SongModel implements Serializable {

    @SerializedName("song")
    String song;

    @Unique
    @SerializedName("url")
    String url;

    @SerializedName("artists")
    String artists;

    @SerializedName("cover_image")
    String coverImage;

    @SerializedName("is_favourite")
    boolean isFavourite;

    public String getSong() {
        return song;
    }

    public String getUrl() {
        return url;
    }

    public String getArtists() {
        return artists;
    }

    public String getCoverImage() {
        return coverImage;
    }

    public boolean isFavourite() {
        return isFavourite;
    }

    public void setFavourite(boolean favourite) {
        isFavourite = favourite;
    }
}
