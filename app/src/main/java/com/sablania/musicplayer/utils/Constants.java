package com.sablania.musicplayer.utils;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.NoConnectionError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.sablania.musicplayer.BuildConfig;
import com.sablania.musicplayer.R;
import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;

public class Constants {

    public static void handleVolleyError(Context context, VolleyError error, boolean showToast){
        if(BuildConfig.DEBUG) {
            if (error.networkResponse != null)
                Log.e("Volley Error Code- ", error.networkResponse.statusCode + "");
            Log.e("Volley Error Msg - ", error.toString() + " in " + context.getClass().getName());
        }
        if (error instanceof NoConnectionError) {
            if(showToast){
                Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
            }
        }else if(error instanceof TimeoutError){
            if(showToast){
                Toast.makeText(context, "Connection Timeout! Please try again.", Toast.LENGTH_SHORT).show();
            }
        }else{
            if(showToast){
                Toast.makeText(context, "Something went wrong. Please try again.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public static void hideInputKeyboard(Activity activity, View view){
        //hide keyboard
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);

        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static void loadImageWithPicasso(final Context context, final ImageView imageView, String url) {
//        Picasso picasso = Picasso.with(context);
//        picasso.setIndicatorsEnabled(true);
//        if (BuildConfig.DEBUG) {
//            picasso.setLoggingEnabled(true);
//            picasso.setDebugging(true);
//        }

        //below commented code doesn't work because picasso directly doesn't support redirected image url loading
//        picasso.load(url)
//                .error(R.drawable.ic_launcher_background)
//                .into(imageView);

        //below is a fix to above code, now redirected images will also be loaded
        Picasso.Builder builder = new Picasso.Builder(context);
//        builder.loggingEnabled(true);
//        builder.debugging(true);
        builder.listener(new Picasso.Listener() {
            @Override
            public void onImageLoadFailed(Picasso picasso, Uri uri, Exception exception) {
                //set error placeholder
                imageView.setImageDrawable(context.getResources()
                                .getDrawable(R.drawable.ic_image_placeholder));
            }
        });
        builder.downloader(new OkHttpDownloader(context));
        builder.build().load(url).into(imageView);
    }
}

