package com.sablania.musicplayer.adapters;

import android.content.Context;
import android.net.Uri;
import android.os.Handler;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.extractor.mp3.Mp3Extractor;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSource;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.google.android.exoplayer2.upstream.TransferListener;
import com.orm.SugarRecord;
import com.sablania.musicplayer.R;
import com.sablania.musicplayer.models.SongModel;
import com.sablania.musicplayer.utils.Constants;

import java.util.ArrayList;
import java.util.List;

public class SongsAdapter extends RecyclerView.Adapter<SongsAdapter.ViewHolder> {

    BottomSheetBehavior bottomSheetBehavior;
    TextView tvPlayingSong, tvPlayingArtists;
    ImageView ivPlay, ivPlay2, ivPrev, ivNext, ivCoverImage;

    Handler handler;
    DataSource.Factory dataSourceFactory;
    TrackSelector trackSelector;
    DefaultLoadControl loadControl;

    int itemPosition;
    String userAgent;
    boolean isPlaying = false, exoplayerStarted = false;

    private Context context;
    private LayoutInflater inflater;
    private ExoPlayer exoPlayer;
    private List<SongModel> list;

    public SongsAdapter(Context context, List<SongModel> list, BottomSheetBehavior bottomSheetBehavior, View bottomSheet) {
        this.context = context;
        this.list = list;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.bottomSheetBehavior = bottomSheetBehavior;

        //bottom sheet element initializaiton
        ivPlay = (ImageView) bottomSheet.findViewById(R.id.iv_play);
        ivPlay2 = (ImageView) bottomSheet.findViewById(R.id.iv_play2);
        ivPrev = (ImageView) bottomSheet.findViewById(R.id.iv_prev);
        ivNext = (ImageView) bottomSheet.findViewById(R.id.iv_next);
        tvPlayingSong = (TextView) bottomSheet.findViewById(R.id.tv_playing_song);
        tvPlayingArtists = (TextView) bottomSheet.findViewById(R.id.tv_playing_artists);
        ivCoverImage = (ImageView) bottomSheet.findViewById(R.id.iv_cover_image);
        setOnClickListeners();

        //set up for exoplayer
        handler = new Handler();
        userAgent = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.11; rv:40.0) Gecko/20100101 Firefox/40.0";
        dataSourceFactory = createDataSourceFactory(context, userAgent, null);

        trackSelector = new DefaultTrackSelector();
        loadControl = new DefaultLoadControl();

        exoPlayer = ExoPlayerFactory.newSimpleInstance(context, trackSelector, loadControl);

    }

    private static DefaultDataSourceFactory createDataSourceFactory(Context context,
                                                                    String userAgent, TransferListener<? super DataSource> listener) {
        // Default parameters, except allowCrossProtocolRedirects is true
        DefaultHttpDataSourceFactory httpDataSourceFactory = new DefaultHttpDataSourceFactory(
                userAgent,
                listener,
                DefaultHttpDataSource.DEFAULT_CONNECT_TIMEOUT_MILLIS,
                DefaultHttpDataSource.DEFAULT_READ_TIMEOUT_MILLIS,
                true /* allowCrossProtocolRedirects */
        );

        DefaultDataSourceFactory dataSourceFactory = new DefaultDataSourceFactory(
                context,
                listener,
                httpDataSourceFactory
        );

        return dataSourceFactory;
    }

    @Override
    public SongsAdapter.ViewHolder onCreateViewHolder(final ViewGroup parent, int viewType) {
        View convertView = inflater.inflate(R.layout.song_item_view, parent, false);

        return new ViewHolder(convertView);
    }

    @Override
    public void onBindViewHolder(final SongsAdapter.ViewHolder holder, final int position) {
        final SongModel songModel = list.get(position);

        holder.tvSong.setText(songModel.getSong());
        holder.tvArtists.setText(songModel.getArtists());
        Constants.loadImageWithPicasso(context, holder.ivCoverImage, songModel.getCoverImage());
        ArrayList<SongModel> temp = (ArrayList<SongModel>) SugarRecord.find(SongModel.class, "url = ?", songModel.getUrl());
        if (!temp.isEmpty() && temp.get(0).isFavourite()) {
            holder.ivFavourite.setImageResource(R.drawable.ic_star);
        } else {
            holder.ivFavourite.setImageResource(R.drawable.ic_star_border);
        }

        holder.ivFavourite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<SongModel> temp = (ArrayList<SongModel>) SugarRecord.find(SongModel.class, "url = ?", songModel.getUrl());
                boolean isFavourite = !temp.isEmpty() && temp.get(0).isFavourite();
                if (!isFavourite) {
                    holder.ivFavourite.setImageResource(R.drawable.ic_star);
                } else {
                    holder.ivFavourite.setImageResource(R.drawable.ic_star_border);
                }
                songModel.setFavourite(!isFavourite);
                SugarRecord.save(songModel);
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemPosition = position;
                playNewSong(songModel);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    private void setOnClickListeners() {
        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isPlaying) {
                    playSong();
                } else {
                    pauseSong();
                }
            }
        };
        ivPlay.setOnClickListener(onClickListener);
        ivPlay2.setOnClickListener(onClickListener);
        ivPrev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemPosition--;
                playNewSong(list.get(itemPosition));
            }
        });
        ivNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemPosition++;
                playNewSong(list.get(itemPosition));
            }
        });

    }

    private void playSong() {
        if (!exoplayerStarted) {
            if (!list.isEmpty()) {
                playNewSong(list.get(0));
            }
            return;
        }
        isPlaying = true;
        exoPlayer.setPlayWhenReady(true);
        ivPlay.setImageResource(R.drawable.ic_pause);
        ivPlay2.setImageResource(R.drawable.ic_pause_circle_filled);
    }

    private void pauseSong() {
        isPlaying = false;
        exoPlayer.setPlayWhenReady(false);
        ivPlay.setImageResource(R.drawable.ic_play_arrow);
        ivPlay2.setImageResource(R.drawable.ic_play_circle_filled);
    }

    private void playNewSong(SongModel songModel) {

        tvPlayingSong.setText(songModel.getSong());
        tvPlayingArtists.setText(songModel.getArtists());

        if (itemPosition == 0) {
            ivPrev.setEnabled(false);
        } else {
            ivPrev.setEnabled(true);
        }
        if (itemPosition == list.size() - 1) {
            ivNext.setEnabled(false);
        } else {
            ivNext.setEnabled(true);
        }

        Constants.loadImageWithPicasso(context, ivCoverImage, songModel.getCoverImage());
        Uri uri = Uri.parse(songModel.getUrl());

        MediaSource mediaSource = new ExtractorMediaSource(uri, dataSourceFactory, Mp3Extractor.FACTORY,
                handler, null);
//        exoPlayer.addListener(this);
        exoPlayer.prepare(mediaSource);
        exoplayerStarted = true;
        playSong();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvSong, tvArtists;
        ImageView ivCoverImage, ivFavourite;

        ViewHolder(View itemView) {
            super(itemView);

            tvSong = (TextView) itemView.findViewById(R.id.tv_song_name);
            tvArtists = (TextView) itemView.findViewById(R.id.tv_artists);
            ivCoverImage = (ImageView) itemView.findViewById(R.id.iv_cover_image);
            ivFavourite = (ImageView) itemView.findViewById(R.id.iv_favourite);
        }
    }

}
