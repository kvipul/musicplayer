# Music Player App
Development Time: 11 Hrs


## Aim
Develop a pseudo-Android application which should list and play best studio songs.

## App Features
- Home page lists all the songs via network request with search functionality and user can mark song as favourite and can see all of them together
- Views are made in such a way that app will work fine in both screen orientations
- Material vector drawables are used for icons which basically use less memory and can be scaled
- GenericRequest is used to cache the api response for a few day, with this app will work in offline mode also (most of the time)
- Ripple effect is also provided for most of the events
- Code is written in separate modules to remove code duplication, maintain better code structure and to make app easily scalable
- I've used Sugar orm for SQLite database, with this history functionality can also be implemented easily
- Proguard is also used for release build to make app size small and to make app more secure

## Third Party libraries and References
- Volley For Network Call
- Data Source: [Ola Play Studios API](http://starlord.hackerearth.com/studio)
- Exoplayer to stream song via network
- Gson for JSON data parsing
- Sugar ORM for SQLite database storage (Instant run doesn't work with this, so disable instant run before running project)
- Picasso and OkHttp for images loading
- Some Files like GenericRequest.java, AppController.java are the files that I've been using for a very long time. Some section of these files may be from internet
 